export interface Environment {
  production: boolean;
  apiBaseUrl: string;
  oauth2RedirectUri: string;
  googleAuthUrl: string;
}
