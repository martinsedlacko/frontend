import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

import { Constructor } from './base';

export interface HasSubscription {
  readonly destroy$: Subject<boolean>;
}

type HasSubscriptionCtor = Constructor<HasSubscription>;

export function WithSubscriptionMixin<Tbase extends Constructor<{}>>(
  superClass: Tbase = class {} as any
): HasSubscriptionCtor & Tbase {
  return class extends superClass implements OnDestroy {
    readonly destroy$: Subject<boolean> = new Subject();

    ngOnDestroy(): void {
      this.destroy$.next(true);
      this.destroy$.complete();
    }

    /* eslint-disable @typescript-eslint/no-unsafe-argument */
    constructor(...args: any[]) {
      super(...args);
    }
  };
}
