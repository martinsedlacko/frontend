export class Base {
  /* eslint-disable @typescript-eslint/no-empty-function */
  constructor() {}
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export type BaseConstructor = new (...args: any[]) => Base;

export type Constructor<T> = new (...args: any[]) => T;
