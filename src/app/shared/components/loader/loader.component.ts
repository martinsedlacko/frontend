import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs';
import { LoaderService } from 'src/app/core';
import { WithSubscriptionMixin } from 'src/app/shared/mixins/has-subscription.mixin';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent extends WithSubscriptionMixin() implements OnInit {
  show = false;

  constructor(private readonly loaderService: LoaderService) {
    super();
  }

  ngOnInit(): void {
    this.loaderService.showLoader$
      .pipe(takeUntil(this.destroy$))
      .subscribe((show) => (this.show = show));
  }
}
