import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  calculateFieldValue(
    sourceObject: object,
    value: string,
    field: string
  ): number | undefined {
    const propertyDescriptor = Reflect.getOwnPropertyDescriptor(
      sourceObject,
      field
    );
    if (propertyDescriptor === undefined || !propertyDescriptor.writable)
      return undefined;

    const fieldNewValue: number = +propertyDescriptor.value + +value;
    if (isNaN(fieldNewValue)) return undefined;

    return fieldNewValue;
  }
}
