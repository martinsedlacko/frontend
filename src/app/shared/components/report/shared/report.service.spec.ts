import { Activity } from 'src/app/shared';

import { ReportService } from './report.service';

describe('ReportServce', () => {
  const reportService: ReportService = new ReportService();
  const activity: Activity = {
    id: 1,
    exercise: 0,
    steps: 1000
  };

  describe('Calculate field value', () => {
    it('should add 20 to Activity.exercise', () => {
      const result: number | undefined = reportService.calculateFieldValue(
        activity,
        '20',
        'exercise'
      );

      expect(result).toBe(20);
    });

    it('should return undefined when value is NaN', () => {
      const result: number | undefined = reportService.calculateFieldValue(
        activity,
        'test',
        'exercise'
      );

      expect(result).toBeUndefined();
    });

    it('should return undefined when property does not exist', () => {
      const result: number | undefined = reportService.calculateFieldValue(
        activity,
        '20',
        'test'
      );

      expect(result).toBeUndefined();
    });
  });
});
