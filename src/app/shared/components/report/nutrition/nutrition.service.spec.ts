import { HttpClient } from '@angular/common/http';
import { TestBed, fakeAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { AuthService, User } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';

import { Nutrition } from './nutrition.model';
import { NutritionService } from './nutrition.service';

describe('NutritionService', () => {
  let nutritionService: NutritionService,
    mockHttp: jasmine.SpyObj<HttpClient>,
    mockAuthService: jasmine.SpyObj<AuthService>;

  const user: User = {
    id: 1,
    name: 'test name',
    email: 'test mail',
    imageUrl: 'test image url',
    emailVerified: true,
    roles: []
  };

  const nutrition: Nutrition = {
    id: 1,
    protein: 150,
    carbohydrates: 300,
    fat: 50,
    calories: 2500,
    fiber: 15
  };

  beforeEach(() => {
    mockHttp = jasmine.createSpyObj<HttpClient>('mockHttp', ['post']);
    mockAuthService = jasmine.createSpyObj<AuthService>('mockAuthService', [
      'getUser'
    ]);

    TestBed.configureTestingModule({
      providers: [
        NutritionService,
        { provide: HttpClient, useValue: mockHttp },
        { provide: ENVIRONMENT_TOKEN, useValue: { apiBaseUrl: '' } },
        { provide: AuthService, useValue: mockAuthService }
      ]
    });

    nutritionService = TestBed.inject(NutritionService);
  });

  describe('findByDate', () => {
    it('should return Nutrition', fakeAsync(() => {
      mockHttp.post.and.returnValue(of(nutrition));
      mockAuthService.getUser.and.returnValue(user);

      let result!: Nutrition;

      nutritionService
        .findByDate(new Date())
        .subscribe((res) => (result = res));

      expect(result).toBe(nutrition);
    }));

    it('findByDate should call http post', () => {
      mockAuthService.getUser.and.returnValue(user);
      const mockPost: jasmine.Spy = mockHttp.post.and.returnValue(
        of(nutrition)
      );

      nutritionService.findByDate(new Date());

      expect(mockPost).toHaveBeenCalledTimes(1);
    });
  });

  describe('save', () => {
    it('should return new Nutrition', fakeAsync(() => {
      mockHttp.post.and.returnValue(of(nutrition));
      mockAuthService.getUser.and.returnValue(user);

      let result!: Nutrition;

      nutritionService
        .save(nutrition, new Date())
        .subscribe((res) => (result = res));

      expect(result).toBe(nutrition);
    }));

    it('save should call http post', () => {
      mockAuthService.getUser.and.returnValue(user);
      const mockPost: jasmine.Spy = mockHttp.post.and.returnValue(
        of(nutrition)
      );

      nutritionService.save(nutrition, new Date());

      expect(mockPost).toHaveBeenCalledTimes(1);
    });
  });
});
