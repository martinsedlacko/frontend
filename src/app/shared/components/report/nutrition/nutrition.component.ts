import { Component, Input, SimpleChanges, OnChanges } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { cloneDeep } from 'lodash-es';
import { ToastrService } from 'ngx-toastr';
import { takeUntil } from 'rxjs';
import {
  Dialog,
  DialogComponent,
  DialogService,
  ReportService,
  Nutrition,
  NutritionService
} from 'src/app/shared';
import { WithSubscriptionMixin } from 'src/app/shared/mixins/has-subscription.mixin';

@Component({
  selector: 'app-nutrition',
  templateUrl: './nutrition.component.html',
  styleUrls: ['./nutrition.component.css', '../shared/styles.css']
})
export class NutritionComponent
  extends WithSubscriptionMixin()
  implements OnChanges
{
  readonly title: string = 'Nutrition';
  readonly proteinLimit: number = 180;
  readonly carbohydratesLimit: number = 400;
  readonly fatLimit: number = 70;
  readonly caloriesLimit: number = 2500;
  readonly fiberLimit: number = 30;

  @Input() date: Date;

  nutritions: Nutrition = {
    id: null,
    protein: 0,
    carbohydrates: 0,
    fat: 0,
    calories: 0,
    fiber: 0
  };

  constructor(
    private readonly nutritionService: NutritionService,
    private readonly dialog: MatDialog,
    private readonly reportService: ReportService,
    private readonly dialogService: DialogService,
    private readonly toastr: ToastrService
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['date'] && changes['date'].currentValue) {
      this.fetchNutrition(this.date);
    }
  }

  openDialog(field: string): void {
    const dialogRef: MatDialogRef<DialogComponent, Dialog> = this.dialog.open<
      DialogComponent,
      Dialog,
      Dialog
    >(
      DialogComponent,
      this.dialogService.getDefaultDialogConfig(field, this.title, true)
    );

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe((result) => {
        if (!result) return;

        field = field.toLowerCase();
        const fieldValue = this.reportService.calculateFieldValue(
          this.nutritions,
          result.value,
          field
        );

        if (fieldValue === undefined) return;

        const newValue: Nutrition = cloneDeep(this.nutritions);
        Reflect.set(newValue, field, fieldValue);

        this.nutritionService.save(newValue, this.date).subscribe({
          next: (res) => {
            this.nutritions = res;
            this.toastr.success('Successfully saved');
          }
        });
      });
  }

  private fetchNutrition(date: Date): void {
    this.nutritionService
      .findByDate(date)
      .subscribe((res) => (this.nutritions = res));
  }
}
