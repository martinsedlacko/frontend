import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';
import { Nutrition } from 'src/app/shared';
import { Environment } from 'src/environments/environment.model';

@Injectable({
  providedIn: 'root'
})
export class NutritionService {
  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
    @Inject(ENVIRONMENT_TOKEN) private readonly environment: Environment
  ) {}

  findByDate(date: Date): Observable<Nutrition> {
    date = new Date(Date.parse(formatDate(date, 'yyyy-MM-dd', 'en')));

    return this.http.post<Nutrition>(
      this.environment.apiBaseUrl + '/nutrition/findByDateAndUser',
      {
        userId: this.authService.getUser().id,
        date: date
      }
    );
  }

  save(nutrition: Nutrition, date: Date): Observable<Nutrition> {
    return this.http.post<Nutrition>(
      this.environment.apiBaseUrl + '/nutrition',
      {
        id: nutrition.id,
        protein: nutrition.protein,
        carbohydrates: nutrition.carbohydrates,
        fat: nutrition.fat,
        calories: nutrition.calories,
        fiber: nutrition.fiber,
        date: date,
        userId: this.authService.getUser().id
      }
    );
  }
}
