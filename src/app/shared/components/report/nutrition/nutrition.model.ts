export interface Nutrition {
  id: number | null;
  protein: number;
  carbohydrates: number;
  fat: number;
  fiber: number;
  calories: number;
}
