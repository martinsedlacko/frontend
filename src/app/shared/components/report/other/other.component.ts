import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { cloneDeep } from 'lodash-es';
import { ToastrService } from 'ngx-toastr';
import { takeUntil } from 'rxjs';
import {
  Other,
  OtherService,
  ReportService,
  DialogService,
  DialogComponent,
  Dialog
} from 'src/app/shared';
import { WithSubscriptionMixin } from 'src/app/shared/mixins/has-subscription.mixin';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.css', '../shared/styles.css']
})
export class OtherComponent
  extends WithSubscriptionMixin()
  implements OnChanges
{
  readonly title: string = 'Other';
  readonly sleepLimit = 8;

  other: Other = {
    id: null,
    sleep: 0
  };

  @Input() date: Date;

  constructor(
    private readonly otherService: OtherService,
    private readonly dialog: MatDialog,
    private readonly reportService: ReportService,
    private readonly dialogService: DialogService,
    private readonly toastr: ToastrService
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['date'] && changes['date'].currentValue) {
      this.fetchOther(this.date);
    }
  }

  openDialog(field: string) {
    const dialogRef: MatDialogRef<DialogComponent, Dialog> = this.dialog.open<
      DialogComponent,
      Dialog,
      Dialog
    >(
      DialogComponent,
      this.dialogService.getDefaultDialogConfig(field, this.title, true)
    );

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe((result) => {
        if (!result) return;

        field = field.toLowerCase();
        const fieldValue = this.reportService.calculateFieldValue(
          this.other,
          result.value,
          field
        );

        if (fieldValue === undefined) return;

        const otherCopy: Other = cloneDeep(this.other);

        Reflect.set(otherCopy, field, fieldValue);
        this.otherService.save(otherCopy, this.date).subscribe({
          next: (res) => {
            this.other = res;
            this.toastr.success('Successfully saved');
          }
        });
      });
  }

  private fetchOther(date: Date) {
    this.otherService.findByDate(date).subscribe((res) => {
      this.other = res;
    });
  }
}
