import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';
import { Other } from 'src/app/shared';
import { Environment } from 'src/environments/environment.model';

@Injectable({
  providedIn: 'root'
})
export class OtherService {
  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
    @Inject(ENVIRONMENT_TOKEN) private readonly environment: Environment
  ) {}

  findByDate(date: Date): Observable<Other> {
    date = new Date(Date.parse(formatDate(date, 'yyyy-MM-dd', 'en')));
    return this.http.post<Other>(
      this.environment.apiBaseUrl + '/other/findByUserAndDate',
      {
        date: date,
        userId: this.authService.getUser().id
      }
    );
  }

  save(other: Other, date: Date): Observable<Other> {
    return this.http.post<Other>(this.environment.apiBaseUrl + '/other', {
      date: date,
      sleep: other.sleep,
      id: other.id,
      userId: this.authService.getUser().id
    });
  }
}
