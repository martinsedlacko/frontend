import { HttpClient } from '@angular/common/http';
import { TestBed, fakeAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { AuthService, User } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';

import { Other } from './other.model';
import { OtherService } from './other.service';

describe('OtherService', () => {
  let otherService: OtherService,
    mockHttp: jasmine.SpyObj<HttpClient>,
    mockAuthService: jasmine.SpyObj<AuthService>;

  const other: Other = {
    id: 1,
    sleep: 8
  };

  const user: User = {
    id: 1,
    name: 'test name',
    email: 'test mail',
    imageUrl: 'test image url',
    emailVerified: true,
    roles: []
  };

  beforeEach(() => {
    mockHttp = jasmine.createSpyObj<HttpClient>('mockHttp', ['post']);
    mockAuthService = jasmine.createSpyObj<AuthService>('mockAuthService', [
      'getUser'
    ]);

    TestBed.configureTestingModule({
      providers: [
        OtherService,
        { provide: ENVIRONMENT_TOKEN, useValue: { apiBaseUrl: '' } },
        { provide: HttpClient, useValue: mockHttp },
        { provide: AuthService, useValue: mockAuthService }
      ]
    });

    otherService = TestBed.inject(OtherService);
  });

  describe('findByDate', () => {
    it('should return Other', fakeAsync(() => {
      mockHttp.post.and.returnValue(of(other));
      mockAuthService.getUser.and.returnValue(user);

      let result!: Other;
      otherService.findByDate(new Date()).subscribe((res) => (result = res));

      expect(result).toBe(other);
    }));

    it('findByDate should call http post', () => {
      const mockPost: jasmine.Spy = mockHttp.post.and.returnValue(of(other));
      mockAuthService.getUser.and.returnValue(user);

      otherService.findByDate(new Date());

      expect(mockPost).toHaveBeenCalledTimes(1);
    });
  });

  describe('save', () => {
    it('should return new Other', fakeAsync(() => {
      mockHttp.post.and.returnValue(of(other));
      mockAuthService.getUser.and.returnValue(user);

      let result!: Other;
      otherService.save(other, new Date()).subscribe((res) => (result = res));

      expect(result).toBe(other);
    }));

    it('save should call http post', () => {
      const mockPost: jasmine.Spy = mockHttp.post.and.returnValue(of(other));
      mockAuthService.getUser.and.returnValue(user);

      otherService.save(other, new Date());

      expect(mockPost).toHaveBeenCalledTimes(1);
    });
  });
});
