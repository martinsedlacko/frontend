import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';
import { Activity } from 'src/app/shared';
import { Environment } from 'src/environments/environment.model';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
    @Inject(ENVIRONMENT_TOKEN) private readonly environment: Environment
  ) {}

  findByDate(date: Date): Observable<Activity> {
    date = new Date(Date.parse(formatDate(date, 'yyyy-MM-dd', 'en')));
    return this.http.post<Activity>(
      this.environment.apiBaseUrl + '/activity/findByUserAndDate',
      {
        date: date,
        userId: this.authService.getUser().id
      }
    );
  }

  save(activity: Activity, date: Date): Observable<Activity> {
    return this.http.post<Activity>(this.environment.apiBaseUrl + '/activity', {
      id: activity.id,
      steps: activity.steps,
      exercise: activity.exercise,
      date: date,
      userId: this.authService.getUser().id
    });
  }
}
