export interface Activity {
  id: number | null;
  steps: number;
  exercise: number;
}
