import { HttpClient } from '@angular/common/http';
import { TestBed, fakeAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { AuthService, User } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';

import { Activity } from './activity.model';
import { ActivityService } from './activity.service';

describe('ActivityService', () => {
  let activityService: ActivityService,
    mockHttp: jasmine.SpyObj<HttpClient>,
    mockAuthService: jasmine.SpyObj<AuthService>;

  const activity: Activity = {
    id: 1,
    steps: 1500,
    exercise: 30
  };

  const user: User = {
    id: 1,
    name: 'test name',
    email: 'test mail',
    imageUrl: 'test image url',
    emailVerified: true,
    roles: []
  };

  beforeEach(() => {
    mockHttp = jasmine.createSpyObj<HttpClient>('mockHttp', ['post']);
    mockAuthService = jasmine.createSpyObj<AuthService>('mockAuthService', [
      'getUser'
    ]);

    TestBed.configureTestingModule({
      providers: [
        ActivityService,
        { provide: HttpClient, useValue: mockHttp },
        { provide: ENVIRONMENT_TOKEN, useValue: { apiBaseUrl: '' } },
        { provide: AuthService, useValue: mockAuthService }
      ]
    });

    activityService = TestBed.inject(ActivityService);
  });

  describe('findByDate', () => {
    it('should return Activity', fakeAsync(() => {
      mockHttp.post.and.returnValue(of(activity));
      mockAuthService.getUser.and.returnValue(user);

      let result!: Activity;
      activityService.findByDate(new Date()).subscribe((res) => (result = res));

      expect(result).toBe(activity);
    }));

    it('findByDate should call http post', () => {
      const mockPost: jasmine.Spy = mockHttp.post.and.returnValue(of(activity));
      mockAuthService.getUser.and.returnValue(user);

      activityService.findByDate(new Date());

      expect(mockPost).toHaveBeenCalledTimes(1);
    });
  });

  describe('save', () => {
    it('should return new Activity', fakeAsync(() => {
      mockHttp.post.and.returnValue(of(activity));
      mockAuthService.getUser.and.returnValue(user);

      let result!: Activity;
      activityService
        .save(activity, new Date())
        .subscribe((res) => (result = res));

      expect(result).toBe(activity);
    }));

    it('save should call http post', () => {
      mockAuthService.getUser.and.returnValue(user);
      const mockPost: jasmine.Spy = mockHttp.post.and.returnValue(of(activity));

      activityService.save(activity, new Date());

      expect(mockPost).toHaveBeenCalledTimes(1);
    });
  });
});
