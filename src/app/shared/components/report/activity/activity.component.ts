import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { cloneDeep } from 'lodash-es';
import { ToastrService } from 'ngx-toastr';
import { Subscription, takeUntil } from 'rxjs';
import {
  DialogService,
  DialogComponent,
  Dialog,
  ReportService,
  ActivityService,
  Activity
} from 'src/app/shared';
import { WithSubscriptionMixin } from 'src/app/shared/mixins/has-subscription.mixin';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css', '../shared/styles.css']
})
export class ActivityComponent
  extends WithSubscriptionMixin()
  implements OnChanges
{
  readonly title: string = 'Activity';
  subs: Subscription;
  @Input() date: Date = new Date();
  activity: Activity = {
    id: null,
    steps: 0,
    exercise: 0
  };

  readonly stepsLimit: number = 10000;
  readonly exerciseLimit: number = 30;

  constructor(
    private readonly dialog: MatDialog,
    private readonly activityService: ActivityService,
    private readonly reportService: ReportService,
    private readonly dialogService: DialogService,
    private readonly toastr: ToastrService
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['date'] && changes['date'].currentValue) {
      this.fetchActivity(this.date);
    }
  }

  openDialog(field: string) {
    const dialogRef: MatDialogRef<DialogComponent, Dialog> = this.dialog.open<
      DialogComponent,
      Dialog,
      Dialog
    >(
      DialogComponent,
      this.dialogService.getDefaultDialogConfig(field, this.title, true)
    );

    this.subs = dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe((result) => {
        if (!result) return;

        field = field.toLowerCase();
        const fieldValue = this.reportService.calculateFieldValue(
          this.activity,
          result.value,
          field
        );

        if (fieldValue === undefined) return;

        const activityCopy: Activity = cloneDeep(this.activity);
        Reflect.set(activityCopy, field, fieldValue);

        this.activityService.save(activityCopy, this.date).subscribe({
          next: (res) => {
            this.activity = res;
            this.toastr.success('Successfully saved');
          }
        });
      });
  }

  private fetchActivity(date: Date): void {
    this.activityService
      .findByDate(date)
      .subscribe((response) => (this.activity = response));
  }
}
