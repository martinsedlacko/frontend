import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs';
import { WithSubscriptionMixin } from 'src/app/shared/mixins/has-subscription.mixin';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent extends WithSubscriptionMixin() implements OnInit {
  readonly dateFormControl: FormControl = new FormControl();
  date: Date = new Date();

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.dateFormControl.setValue(this.date);
    this.dateFormControl.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value: Date) => {
        this.date = value;
      });
  }
}
