export interface Dialog {
  title: string;
  field: string;
  value: string;
  numberOnly: boolean;
}
