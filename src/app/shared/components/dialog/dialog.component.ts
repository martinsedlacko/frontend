import { Component, ElementRef, Inject,  ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Dialog } from 'src/app/shared';

@Component({
  selector: 'shared-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})
export class DialogComponent {
  @ViewChild("dialogInput")
  private readonly dialogInput: ElementRef<HTMLInputElement>;

  constructor(
    private readonly dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly data: Dialog
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onFocus(): void {
    this.dialogInput.nativeElement.select();
  }
}
