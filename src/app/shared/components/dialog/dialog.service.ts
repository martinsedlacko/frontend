import { Injectable } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { Dialog } from 'src/app/shared';

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  public getDefaultDialogConfig(
    field: string,
    title: string,
    numberOnly = false
  ): MatDialogConfig<Dialog> {
    const config: MatDialogConfig<Dialog> = new MatDialogConfig<Dialog>();
    config.data = {
      field: field,
      value: numberOnly ? '0' : '',
      title: title,
      numberOnly: numberOnly
    };
    config.width = '30%';
    return config;
  }
}
