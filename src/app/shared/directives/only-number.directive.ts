import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyNumber]',
})
export class OnlyNumberDirective {

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    if (
      [
        'Delete',
        'Backspace',
        'Escape',
        'Enter',
        'decimal point',
        'Period',
        'NumpadSubtract',
      ].indexOf(event.code) > 0 ||
      (event.key === 'a' && event.ctrlKey === true) || // Allow: Ctrl+A
      (event.key === 'c' && event.ctrlKey === true) || // Allow: Ctrl+C
      (event.key === 'v' && event.ctrlKey === true) || // Allow: Ctrl+V
      (event.key === 'x' && event.ctrlKey === true) || // Allow: Ctrl+X
      (event.key === 'a' && event.metaKey === true) || // Cmd+A (Mac)
      (event.key === 'c' && event.metaKey === true) || // Cmd+C (Mac)
      (event.key === 'v' && event.metaKey === true) || // Cmd+V (Mac)
      (event.key === 'x' && event.metaKey === true) // Cmd+X (Mac)
    ) {
      return; // let it happen, don't do anything
    }
    // Ensure that it is a number and stop the keypress
    if (event.key === ' ' || isNaN(Number(event.key))) {
      event.preventDefault();
    }
  }
}
