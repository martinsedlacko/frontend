import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { OnlyNumberDirective } from './only-number.directive';

@Component({
  template: `<input appOnlyNumber />`
})
class TestOnlyNumbersDirectiveComponent {}

describe('OnlyNumberDirective', () => {
  let fixture: ComponentFixture<TestOnlyNumbersDirectiveComponent>,
    inputEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestOnlyNumbersDirectiveComponent, OnlyNumberDirective]
    });

    fixture = TestBed.createComponent(TestOnlyNumbersDirectiveComponent);
    inputEl = fixture.debugElement.query(By.directive(OnlyNumberDirective));
    fixture.detectChanges();
  });

  it('if key down event is number then defaultPrevented should be false', () => {
    const event = new KeyboardEvent('keydown', { key: '5' });
    const preventDefaultSpy: jasmine.Spy = spyOn(event, 'preventDefault');

    (inputEl.nativeElement as HTMLInputElement).dispatchEvent(event);

    expect(preventDefaultSpy).not.toHaveBeenCalled();
  });

  it('if key down event is not a number, then defaultPrevented should be true', () => {
    const event = new KeyboardEvent('keydown', { key: 'f' });
    const preventDefaultSpy: jasmine.Spy = spyOn(event, 'preventDefault');

    (inputEl.nativeElement as HTMLInputElement).dispatchEvent(event);

    expect(preventDefaultSpy).toHaveBeenCalledOnceWith();
  });
});
