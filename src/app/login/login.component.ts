import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';
import { Environment } from 'src/environments/environment.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  readonly googleAuthUrl: string = this.environment.googleAuthUrl;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly authService: AuthService,
    @Inject(ENVIRONMENT_TOKEN) private readonly environment: Environment
  ) {}

  ngOnInit(): void {
    let token = '';

    this.route.queryParams.subscribe((params) => {
      token = params['token'] as string;
      return token;
    });

    if (token) {
      this.authService.login(token).subscribe((resp) => {
        if (!resp) {
          this.router.navigate(['/login']);
        } else {
          this.router.navigate(['/']);
        }
      });
      return;
    } else {
      this.router.navigate(['/login']);
      return;
    }
  }
}
