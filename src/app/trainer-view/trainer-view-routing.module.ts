import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrainerGuard, AuthGuard } from 'src/app/core';
import {
  ClientListComponent,
  TrainerViewComponent,
} from 'src/app/trainer-view';

const routes: Routes = [
  {
    path: '',
    component: TrainerViewComponent,
    canActivate: [AuthGuard, TrainerGuard],
    children: [
      {
        path: 'client-list',
        component: ClientListComponent,
        canActivate: [AuthGuard, TrainerGuard],
      },
    ],
  },
  { path: '**', redirectTo: 'client-list' },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrainerViewRoutingModule {
  static components = [TrainerViewComponent, ClientListComponent];
}
