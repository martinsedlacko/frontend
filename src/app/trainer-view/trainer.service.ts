import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User, ApiResponsePageable, Client } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';
import { Environment } from 'src/environments/environment.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  constructor(
    private readonly http: HttpClient,
    @Inject(ENVIRONMENT_TOKEN) private readonly environment: Environment
  ) {}

  public findAllClientByTrainerId(trainerId: number): Observable<User[]> {
    return this.http.get<User[]>(
      `${this.environment.apiBaseUrl}/client/findAllByTrainerId/${trainerId}`
    );
  }

  public findAllClientByTrainerIdPageable(
    trainerId: number,
    offset: number,
    pageSize: number
  ): Observable<ApiResponsePageable<Client[]>> {
    return this.http.get<ApiResponsePageable<Client[]>>(
      `${this.environment.apiBaseUrl}/client/findAllByTrainerId/${trainerId}/${pageSize}/${offset}`
    );
  }
}
