import { HttpClient } from '@angular/common/http';
import { TestBed, fakeAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { Client } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';

import { ApiResponsePageable } from './../core/model/api-response.model';
import { User } from './../core/model/user.model';
import { TrainerService } from './trainer.service';

describe('TrainerService', () => {
  let trainerService: TrainerService, mockHttp: jasmine.SpyObj<HttpClient>;

  const user: User = {
    id: 1,
    name: 'test name',
    email: 'test mail',
    imageUrl: 'test image url',
    emailVerified: true,
    roles: []
  };

  const data: ApiResponsePageable<Client[]> = {
    content: [
      {
        id: 1,
        trainer: user,
        user: user
      }
    ],
    totalElements: 1,
    totalPages: 1
  };

  beforeEach(() => {
    mockHttp = jasmine.createSpyObj<HttpClient>('mockHttp', ['get']);

    TestBed.configureTestingModule({
      providers: [
        TrainerService,
        { provide: HttpClient, useValue: mockHttp },
        { provide: ENVIRONMENT_TOKEN, useValue: { apiBaseUrl: '' } }
      ]
    });

    trainerService = TestBed.inject(TrainerService);
  });

  describe('find all clients by trainer id pageable', () => {
    it('should return pageable clients', fakeAsync(() => {
      mockHttp.get.and.returnValue(of(data));

      let result!: ApiResponsePageable<Client[]>;
      trainerService
        .findAllClientByTrainerIdPageable(1, 1, 1)
        .subscribe((res) => (result = res));

      expect(result).toBe(data);
    }));

    it('should call http get', () => {
      const mockGet: jasmine.Spy = mockHttp.get.and.returnValue(of(data));

      trainerService.findAllClientByTrainerIdPageable(1, 1, 1);

      expect(mockGet).toHaveBeenCalledTimes(1);
    });
  });
});
