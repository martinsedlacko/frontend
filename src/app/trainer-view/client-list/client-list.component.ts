import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { map, startWith, switchMap, takeUntil } from 'rxjs';
import { User, AuthService } from 'src/app/core';
import { Dialog, DialogComponent, DialogService } from 'src/app/shared';
import { WithSubscriptionMixin } from 'src/app/shared/mixins/has-subscription.mixin';
import { TrainerService } from 'src/app/trainer-view';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent
  extends WithSubscriptionMixin()
  implements AfterViewInit
{
  readonly displayedColumns: Set<keyof User> = new Set(['id', 'name', 'email']);
  readonly pageSizeOptions: number[] = [5, 10, 20, 50];

  dataSource: MatTableDataSource<User> = new MatTableDataSource();
  resultsLength = 3;
  isLoadingResults = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly trainerService: TrainerService,
    private readonly authService: AuthService,
    private readonly dialog: MatDialog,
    private readonly dialogService: DialogService
  ) {
    super();
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.trainerService.findAllClientByTrainerIdPageable(
            this.authService.getUser().id,
            this.paginator.pageIndex,
            this.paginator.pageSize
          );
        }),
        map((data) => {
          this.isLoadingResults = false;
          if (data === null) return [];

          this.resultsLength = data.totalElements;
          return data.content.map((res) => res.user);
        })
      )
      .subscribe((data) => (this.dataSource.data = data));
  }

  applyFilter(event: KeyboardEvent): void {
    const filterValue: string = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog() {
    const dialogRef: MatDialogRef<DialogComponent, Dialog> = this.dialog.open<
      DialogComponent,
      Dialog,
      Dialog
    >(
      DialogComponent,
      this.dialogService.getDefaultDialogConfig('Email', 'Add client')
    );

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe((result) => {
        if (!result) return;

        console.log(result);
      });
  }
}
