// components
export * from './trainer-view.component';
export * from './client-list/client-list.component';

// services
export * from './trainer.service';
