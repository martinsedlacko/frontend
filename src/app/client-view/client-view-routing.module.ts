import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/core';
import { ReportComponent } from 'src/app/shared';

import { ClientViewComponent } from './client-view.component';


const routes: Routes = [
  {
    path: '',
    component: ClientViewComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'report', component: ReportComponent, canActivate: [AuthGuard] },
    ],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientViewRoutingModule {
  static components = [ClientViewComponent];
}
