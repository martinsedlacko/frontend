import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { ClientViewRoutingModule } from './client-view-routing.module';

@NgModule({
  declarations: [ClientViewRoutingModule.components],
  imports: [CommonModule, ClientViewRoutingModule, SharedModule],
})
export class ClientViewModule {}
