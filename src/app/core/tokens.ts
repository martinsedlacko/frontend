import { InjectionToken } from '@angular/core';
import { Environment } from 'src/environments/environment.model';

export const ENVIRONMENT_TOKEN = new InjectionToken<Environment>('environment');
export const LOCAL_STORAGE_TOKEN = new InjectionToken<Storage>('localStorage');
