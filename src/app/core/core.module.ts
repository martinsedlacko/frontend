import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import {
  ToolbarComponent,
  ServerErrorInterceptor,
  AuthInterceptor,
  LoaderInterceptor,
  GlobalErrorHandler
} from 'src/app/core';

import { LOCAL_STORAGE_TOKEN } from './tokens';

@NgModule({
  declarations: [ToolbarComponent],
  imports: [CommonModule, MatToolbarModule, FlexLayoutModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorInterceptor,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    { provide: LOCAL_STORAGE_TOKEN, useValue: localStorage }
  ],
  exports: [ToolbarComponent]
})
export class CoreModule {}
