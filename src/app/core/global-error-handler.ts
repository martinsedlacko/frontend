import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ErrorService, LoggingService } from 'src/app/core';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private readonly injector: Injector) {}

  handleError(error: Error | HttpErrorResponse) {
    // Need to get Services from injector rather than constructor injection to avoid cyclic dependency error
    const errorService = this.injector.get(ErrorService);
    const logger = this.injector.get(LoggingService);
    const notifier = this.injector.get(ToastrService);

    let message: string;
    let stackTrace: string;

    if (error instanceof HttpErrorResponse) {
      // Server error
      message = errorService.getServerMessage(error);
      console.log(error);
      stackTrace = errorService.getServerStack(error);
      notifier.error(message, undefined, { onActivateTick: true });
    } else {
      // Client Error
      message = errorService.getClientMessage(error);
      stackTrace = errorService.getClientStack(error);
      notifier.error(message, undefined, { onActivateTick: true });
    }

    logger.logError(message, stackTrace);
  }
}
