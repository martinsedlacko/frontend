import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core';

@Injectable({
  providedIn: 'root',
})
export class TrainerGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly toastr: ToastrService
  ) {}

  canActivate(): boolean {
    if (this.authService.isTrainer()) {
      return true;
    } else {
      this.toastr.error('You have insufficient rights.');
      return false;
    }
  }
}
