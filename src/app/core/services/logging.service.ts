import { Inject, Injectable } from '@angular/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';
import { Environment } from 'src/environments/environment.model';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {
  constructor(
    @Inject(ENVIRONMENT_TOKEN) private readonly environment: Environment
  ) {}

  logError(message: string, stack: string) {
    console.error(`Error message : ${message}`);
    if (!this.environment.production) {
      console.error(`Error stack : ${stack}`);
    }
  }
}
