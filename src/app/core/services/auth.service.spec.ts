import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { User, AuthService } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';
import { Environment } from 'src/environments/environment.model';

import { LOCAL_STORAGE_TOKEN } from './../tokens';

describe('AuthService', () => {
  let authService: AuthService,
    mockHttp: jasmine.SpyObj<HttpClient>,
    mockStorage: jasmine.SpyObj<Storage>,
    user: User,
    token: string,
    mockEnv: Environment;

  beforeEach(() => {
    if (!!localStorage.getItem('token')) localStorage.removeItem('token');
    mockEnv = { apiBaseUrl: 'http://localhost:8080' } as Environment;
    mockHttp = jasmine.createSpyObj<HttpClient>('mockHttp', ['get']);
    mockStorage = jasmine.createSpyObj<Storage>('mockStorage', [
      'getItem',
      'setItem',
      'removeItem'
    ]);
    token = 'test token';
    user = {
      id: 1,
      name: 'test name',
      email: 'test mail',
      imageUrl: 'test image url',
      emailVerified: true,
      roles: []
    };

    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: HttpClient, useValue: mockHttp },
        { provide: ENVIRONMENT_TOKEN, useValue: mockEnv },
        { provide: LOCAL_STORAGE_TOKEN, useValue: mockStorage }
      ]
    });
    authService = TestBed.inject(AuthService);
  });

  describe('login', () => {
    it('should set up user', fakeAsync(() => {
      mockHttp.get.and.returnValue(of(user));

      let result!: User | boolean;
      authService.login(token).subscribe((res) => (result = res));

      expect(result).toEqual(user);
    }));

    it('should save token in LocalStorage', () => {
      mockHttp.get.and.returnValue(of(user));
      const setItemSpy: jasmine.Spy = mockStorage.setItem.and.returnValue();

      authService.login(token);

      expect(setItemSpy).toHaveBeenCalledOnceWith('token', token);
    });

    it('should call http.get with the right URL', () => {
      const getMock = mockHttp.get.and.returnValue(of(user));

      authService.login(token);

      expect(getMock).toHaveBeenCalledWith(
        mockEnv.apiBaseUrl + '/user/me',
        jasmine.any(Object)
      );
    });
  });

  describe('checkAuthenticationStatus', () => {
    it('should set user variable in AuthService', fakeAsync(() => {
      mockHttp.get.and.returnValue(of(user));

      authService.checkAuthenticationStatus();

      expect(authService.getUser()).toEqual(user);
    }));
  });

  describe('logout', () => {
    it('should remove token from localstorage', () => {
      const removeItemSpy: jasmine.Spy =
        mockStorage.removeItem.and.returnValue();

      authService.logout();

      expect(removeItemSpy).toHaveBeenCalledOnceWith('token');
    });
  });
});
