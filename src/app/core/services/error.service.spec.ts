import { HttpErrorResponse } from '@angular/common/http';

import { ApiError } from '../model/api-error.model';

import { ErrorService } from './error.service';

describe('ErrorService', () => {
  let errorService: ErrorService,
    serverMessage: HttpErrorResponse,
    clientMessage: Error;

  beforeEach(() => {
    errorService = new ErrorService();

    clientMessage = {
      name: 'Client error',
      message: 'client error message',
      stack: undefined
    };

    serverMessage = {
      error: {},
      message: 'server error message'
    } as HttpErrorResponse;
  });

  describe('getClientMessage', () => {
    it('should return message from client Error', () => {
      const result: string = errorService.getClientMessage(clientMessage);

      expect(result).toBe(clientMessage.message);
    });

    it('should return stack from client Error', () => {
      clientMessage.stack = 'client error stack';

      const result: string = errorService.getClientStack(clientMessage);

      expect(result).toBe(clientMessage.stack);
    });

    it('should return error as string when error.stack is undefined', () => {
      const result: string = errorService.getClientStack(clientMessage);

      expect(result).toBe(clientMessage.toString());
    });
  });

  describe('getServerMessage', () => {
    it('should return api error message from HttpErrorResponse', () => {
      (serverMessage.error as ApiError).message = 'server ApiError message';

      const result: string = errorService.getServerMessage(serverMessage);

      expect(result).toBe((serverMessage.error as ApiError).message);
    });

    it('should return error message from HttpErrorResponse when api error message is null', () => {
      const result: string = errorService.getServerMessage(serverMessage);

      expect(result).toBe(serverMessage.message);
    });
  });
});
