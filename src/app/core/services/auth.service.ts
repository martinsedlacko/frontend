import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, of, Subject, catchError, tap } from 'rxjs';
import { User } from 'src/app/core';
import { ENVIRONMENT_TOKEN } from 'src/app/core/tokens';
import { Environment } from 'src/environments/environment.model';

import { LOCAL_STORAGE_TOKEN } from './../tokens';

const trainerRoleCode = 'TRAINER';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly authChanged$: Subject<boolean> = new Subject<boolean>();
  private readonly getUserUrl: string =
    this.environment.apiBaseUrl + '/user/me';
  private readonly tokenItemName = 'token';
  private user: User;

  isLoggedIn$: Observable<boolean> = this.authChanged$.asObservable();

  constructor(
    private readonly http: HttpClient,
    @Inject(LOCAL_STORAGE_TOKEN) private readonly localStorage: Storage,
    @Inject(ENVIRONMENT_TOKEN) private readonly environment: Environment
  ) {}

  login(token: string): Observable<User | boolean> {
    this.localStorage.setItem(this.tokenItemName, token);

    return this.http.get<User>(this.getUserUrl, {}).pipe(
      tap((user) => {
        this.user = user;
        this.userAuthChanged(true);
      }),
      catchError(() => {
        return of(false);
      })
    );
  }

  checkAuthenticationStatus(): void {
    this.http
      .get<User>(this.getUserUrl)
      .pipe(
        tap((user) => {
          this.user = user;
          this.userAuthChanged(true);
        }),
        catchError(() => of(false))
      )
      .subscribe();
  }

  logout() {
    this.localStorage.removeItem(this.tokenItemName);
    this.userAuthChanged(false);
  }

  getUser(): User {
    return this.user;
  }

  isAuthenticated(): boolean {
    return !!this.user;
  }

  getToken(): string {
    return this.localStorage.getItem(this.tokenItemName) ?? '';
  }

  isTrainer(): boolean {
    return this.user.roles
      .map((role) => role.code.toUpperCase())
      .includes(trainerRoleCode);
  }

  private userAuthChanged(status: boolean) {
    this.authChanged$.next(status);
  }
}
