import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ApiError } from './../model/api-error.model';

@Injectable({
  providedIn: 'root',
})
export class ErrorService {
  getClientMessage(error: Error): string {
    if (!navigator.onLine) {
      return 'No Internet Connection';
    }
    return error.message ? error.message : error.toString();
  }

  getClientStack(error: Error): string {
    return error.stack ? error.stack : error.toString();
  }

  getServerMessage(error: HttpErrorResponse): string {
    const apiError: ApiError = error.error as ApiError;
    return apiError.message ? apiError.message : error.message;
  }

  getServerStack(error: HttpErrorResponse): string {
    // TODO: handle stack trace
    return 'stack';
  }
}
