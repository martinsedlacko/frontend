import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  showLoader$: Subject<boolean> = new Subject<boolean>();

  show(): void {
    this.showLoader$.next(true);
  }

  hide(): void {
    this.showLoader$.next(false);
  }
}
