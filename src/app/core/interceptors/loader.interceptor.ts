import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { finalize, Observable } from 'rxjs';
import { LoaderService } from 'src/app/core';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  private counter = 0;
  constructor(private readonly injector: Injector) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const loaderService: LoaderService = this.injector.get(LoaderService);
    this.counter++;
    if (this.counter === 1) {
      loaderService.show();
    }
    return next.handle(request).pipe(
      finalize(() => {
        loaderService.hide();
        this.counter--;
      })
    );
  }
}
