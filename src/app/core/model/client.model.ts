import { User } from './user.model';

export interface Client {
  id: number;
  trainer: User;
  user: User;
}
