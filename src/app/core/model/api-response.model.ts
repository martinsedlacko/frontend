export interface ApiResponsePageable<E> {
  content: E;
  totalElements: number;
  totalPages: number;
}
