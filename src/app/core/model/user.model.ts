export interface User {
  id: number;
  name: string;
  email: string;
  imageUrl: string;
  emailVerified: boolean;
  roles: Role[];
}

export interface Role {
  id: number;
  code: string
}
