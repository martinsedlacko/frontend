import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, TrainerGuard } from 'src/app/core';

import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: 'oauth2/redirect', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'client-view',
    loadChildren: () =>
      import('./client-view/client-view.module').then(
        (m) => m.ClientViewModule
      ),
    canActivate: [AuthGuard]
  },
  {
    path: 'trainer-view',
    loadChildren: () =>
      import('./trainer-view/trainer-view.module').then(
        (m) => m.TrainerViewModule
      ),
    canActivate: [AuthGuard, TrainerGuard]
  },
  { path: '**', redirectTo: '/client-view' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
