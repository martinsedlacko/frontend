import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs';

import { AuthService, User } from './core';
import { WithSubscriptionMixin } from './shared/mixins/has-subscription.mixin';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends WithSubscriptionMixin() implements OnInit {
  readonly title = 'fit-app';
  isLoggedIn = false;
  user: User;
  roles: string[] = [];

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {
    super();
  }

  ngOnInit() {
    this.authService.checkAuthenticationStatus();
    this.authService.isLoggedIn$
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.isLoggedIn = res;
        if (this.isLoggedIn) {
          this.user = this.authService.getUser();
          this.roles = this.user.roles.map((role) => role.code);
        }
      });
  }

  logout(): void {
    this.authService.logout();
    this.isLoggedIn = false;
    this.router.navigate(['/login']);
  }
}
