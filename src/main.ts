import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Environment } from 'src/environments/environment.model';

import { AppModule } from './app/app.module';
import { ENVIRONMENT_TOKEN } from './app/core/tokens';

fetch('assets/env.json')
  .then((resp) => resp.json())
  .then((env: Environment) => {
    if (env.production) {
      enableProdMode();
    }

    platformBrowserDynamic([{ provide: ENVIRONMENT_TOKEN, useValue: env }])
      .bootstrapModule(AppModule)
      .catch((err) => console.error(err));
  });
